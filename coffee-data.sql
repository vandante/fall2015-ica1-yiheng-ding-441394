CREATE DATABASE `coffee`;
CREATE TABLE `coffee`.`coffees` ( `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,  `name` VARCHAR(50) NOT NULL ,    PRIMARY KEY  (`id`)) ENGINE = InnoDB;
CREATE TABLE `coffee`.`ratings` ( `coffeeId` INT(10) UNSIGNED NOT NULL ,  `user` VARCHAR(50) NOT NULL ,  `rating` DECIMAL(1,1) NOT NULL ,  `comment` TINYTEXT NOT NULL ) ENGINE = InnoDB;
ALTER TABLE `ratings` ADD INDEX(`coffeeId`);
ALTER TABLE `ratings` ADD CONSTRAINT `idconstraint` FOREIGN KEY (`coffeeId`) REFERENCES `coffee`.`coffees`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;